const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/clientes',(req,res)=>{
    let source = req.query;
    let ret = "Dados enviados:" + source.nome;
    res.send("{message:"+ret+"}");
});

app.post('/clientes',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);
    let ret = "Dados enviados: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    ret+=" Idade: " + dados.idade;

    if (!headers_)
        return res.status(400).end()

    res.send("{message:"+ret+"}");


});