const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});


app.get('/funcionarios',(req,res)=>{
    let source = req.query;
    let ret = "Dados enviados:" + source.nome;
    res.send("{message:"+ret+"}");
});

app.delete('/funcionarios',(req, res)=>{
    let dado = req.params.valor;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);

    if (!headers_)
        return res.status(400).end()

    let ret = "Dados solicitado:" + dado;
    res.send("{message:"+ret+"}");
});


app.put('/funcionarios',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);
    let ret = "Dados enviados: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    ret+=" Idade: " + dados.idade;
    res.send("{message:"+ret+"}");
});